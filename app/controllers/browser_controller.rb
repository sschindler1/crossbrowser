class BrowserController < ApplicationController
  #before_filter :access
  
  def show
    
  @numberofvisitorsfingerprint = Machine.distinct.count(:canvas)  
  
  @machine = Machine.all
  respond_to do |format|
         format.json { render json: @machine, :callback => params[:jsoncallback], :content_type => "text/javascript"}  
  end
end
  
  
  def index
    @numberofvisitorsfingerprint = Machine.distinct.count(:canvas) 
    
    @output = Machine.all
    @attribute = Attibute.all
    @cookieshow = User.all
    
    respond_to do |format|
      format.html
      format.js{
        
        
        
        if cookies[:cookie] == nil
        @cookies =cookies.permanent[:cookie] = rand(10000000000000000)
        
        entry = User.new(:cookie => @cookies) 
        entry.save 
        end
        
        begin
        @machine=Machine.find(params[:browser])
        
        
        
        
        if @machine.nil?
          @machine=Machine.create(:browser=>params[:browser])
          @machine_found=false
        else
          
          @machine_found=true
        end
      rescue
        @machine=Machine.create(:browser=>params[:browser],:canvas=>params[:canvas])
        @machine_found=false
      end
      
      render json: @output, callback: params[:jsoncallback],  :content_type => "text/javascript"
    
      }
    
  end
    
  end
  #protected
  #def access
  #authenticate_or_request_with_http_basic do |username, password|
   # username == "test" &&
    #password == "test"
    #end
    #end  
end
