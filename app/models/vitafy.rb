class Vitafy < ActiveRecord::Base
  validates_uniqueness_of :piclink
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
end
