require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get tag" do
    get :tag
    assert_response :success
  end

  test "should get analyse" do
    get :analyse
    assert_response :success
  end

end
