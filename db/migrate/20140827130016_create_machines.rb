class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.text :browser,  limit: 4
      t.text :canvas,  limit: 4
      t.text :title,  limit: 4
      t.text :link,  limit: 4
      t.text :ident,  limit: 4

      t.timestamps
    end
  end
end
