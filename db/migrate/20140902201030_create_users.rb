class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text :browser,  limit: 4
      t.text :canvas,  limit: 4
      t.text :cookie,  limit: 4
      t.text :identifier1,  limit: 4

      t.timestamps
    end
  end
end
