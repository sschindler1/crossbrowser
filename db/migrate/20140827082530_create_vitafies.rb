class CreateVitafies < ActiveRecord::Migration
  def change
    create_table :vitafies do |t|
      t.text :piclink,  limit: 4
      t.text :link,  limit: 4
      t.text :price,  limit: 4
      t.text :tag1,  limit: 4
      t.text :tag2,  limit: 4
      t.text :tag3,  limit: 4
      t.text :title,  limit: 4
      t.text :description,  limit: 4
      t.text :tag4,  limit: 4

      t.timestamps
    end
  end
end
